#!/usr/bin/env perl
use Mojolicious::Lite;

use DBI;
use DBD::mysql;
use POSIX;

my $dbh = DBI->connect("dbi:mysql:scrabble:localhost:3306","root","root") or die "Unable to connect: $DBI::errstr\n";

get '/' => sub {
  my $c = shift;
  $c->render(template => 'index');
};

get '/viewmember' => sub {
  my $c  = shift;
  my $id = $c->param('id');

  my $sql = $dbh->prepare("SELECT * FROM players WHERE id = " . $id);
  $sql->execute;
  my @member = $sql->fetchrow;

  $c->render(template => 'member', first_name => $member[1], last_name => $member[2], id => $id);

};

post '/new-score' => sub {
  my $c  = shift;
  my $p1 = $c->param('player_1');
  my $p2 = $c->param('player_2');
  my $p1_score = $c->param('score_1');
  my $p2_score = $c->param('score_2');

  my $sql = $dbh->prepare("SELECT max(id) FROM games");
  $sql->execute;
  my @row = $sql->fetchrow;
  my $id = $row[0];
  $id++;

  my $insert = eval { $dbh->prepare('INSERT INTO games VALUES (?,?,?,?,?,?)') };
  $insert->execute($id, $p1, $p2, $p1_score, $p2_score, strftime "%Y-%m-%d %H:%M:%S", gmtime time);

  $c->redirect_to('/');

};

sub calcWinLossRatio {

  my ($wins, $losses) = @_;

  if($losses != 0 && $wins != 0){
    while($wins % 2 == 0 && $losses % 2 == 0){

      $wins = $wins/2;
      $losses = $losses/2;
    }
  }

  my $ratio = "$wins:$losses";

  return $ratio;

}

sub getPlayerAvg {

  my @scores = @_;
  my $overall = 0;

  foreach my $score (@scores){

    $overall = $overall + $score;

  }

  my $length = @scores;
  my $avg = sprintf("%.2f", $overall/$length);

  return $avg;

}

sub getPlayerHighest {

  my @scores = shift;
  my $highest = 0;

  foreach my $score (@scores){

    if($score > $highest){
      $highest = $score;
    }

  }

  return $highest;

}


helper db => sub { $dbh };
helper calcWinLossRatio => sub { shift; calcWinLossRatio(@_) };
helper getPlayerAvg => sub { shift; getPlayerAvg(@_) };
helper getPlayerHighest => sub { shift; getPlayerHighest(@_) };

app->start;
__DATA__

@@ index.html.ep
% layout 'default';
% title 'Scrabble';
<h1>Luke's Scrabble Club</h1>

@@ member.html.ep
% layout 'member';
% title 'Scrabble';

@@ layouts/default.html.ep

% my $players = db->prepare("SELECT * FROM players");
% $players->execute;
<!DOCTYPE html>
<html>
  <head><title><%= title %></title></head>
  <link rel="stylesheet" type="text/css" href="/css/main.css">
  <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

  <div class='container'>
  <body><%= content %></body>
    <br><br>
    <h1>User Stats</h1>
    <table class='table table-striped'>
      <thead>
        <tr>
          <th>First name</th>
          <th>Last name</th>
          <th>Score this week</th>
          <th>Score to date</th>
          <th>Games played</th>
          <th>Win/Loss Ratio</th>
          <th>View Profile</th>
        </tr>
      </thead>
      <tbody>
        % while(my @row = $players->fetchrow ) {
        <tr>
        % my $gamesPlayed  = 0;
        % my $scoreWeekly  = 0;
        % my $scoreToDate  = 0;
        % my $wins         = 0;
        % my $losses       = 0;
        % my $games        = db->prepare("SELECT * FROM games");
        % my $gamesWeekly = db->prepare("SELECT * FROM games WHERE date between date_sub(now(),INTERVAL 1 WEEK) and now()");
        % $games->execute;
        % $gamesWeekly->execute;

          % while(my @gamesRow = $games->fetchrow) {

            % if($gamesRow[1] == $row[0]){
                % $gamesPlayed = $gamesPlayed + 1;
                % $scoreToDate = $scoreToDate + $gamesRow[3];
                % if($gamesRow[3] > $gamesRow[4]){
                  % $wins++;
                % } else {
                %   $losses++;
                % }
            % } elsif ($gamesRow[2] == $row[0]) {
              % $gamesPlayed = $gamesPlayed + 1;
              % $scoreToDate = $scoreToDate + $gamesRow[4];
              % if($gamesRow[4] > $gamesRow[3]){
                % $wins++;
              % } else {
              %   $losses++;
              % }
            % }
          % }

          % while(my @gamesRowWeekly = $gamesWeekly->fetchrow) {


            % if($gamesRowWeekly[1] == $row[0]){
                % $scoreWeekly = $scoreWeekly + $gamesRowWeekly[3];
            % } elsif ($gamesRowWeekly[2] == $row[0]){
              % $scoreWeekly = $scoreWeekly + $gamesRowWeekly[4];
            % }

          % }

          % my $winLossRatio = calcWinLossRatio($wins, $losses);

            <td><%= $row[1] %></td>
            <td><%= $row[2] %></td>
            <td><%= $scoreWeekly %></td>
            <td><%= $scoreToDate %></td>
            <td><%= $gamesPlayed %></td>
            <td><%= $winLossRatio %></td>
            <td><a href='/viewmember?id=<%= $row[0] %>'> View</a>

        </tr>
        % }
      </tbody>
    </table>

    <h1>Add a game: </h1>

    <form action='/new-score' method='POST' >
    <label for='player_1'>Player One: </label>
    % $players->execute;
    <select id='player_1' name='player_1'>
    % while(my @row = $players->fetchrow ) {
      <option value="<%= $row[0] %>"><%= $row[1] . ' ' . $row[2] %></option>
    % }
    </select><br><br>
    <label for='player_1'>Player Two: </label>
    % $players->execute;
    <select id='player_2' name='player_2'>
    % while(my @row = $players->fetchrow ) {
      <option value="<%= $row[0] %>"><%= $row[1] . ' ' . $row[2] %></option>
    % }
    </select><br><br>
    <label for='score_1'>Score Player 1: </label>
    <input type='number' id='score_1' name='score_1'><br><br>
    <label for='score_2'>Score Player 2: </label>
    <input type='number' id='score_2' name='score_2'>
    <br><br>
    <input type='submit' value='submit'>
  </div>

  </div>


</html>

@@ layouts/member.html.ep

<head><title><%= title %></title></head>
<link rel="stylesheet" type="text/css" href="/css/main.css">
<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<div class='container'>
<br><br>
<h1 class='text-center'><%= $first_name %> <%= $last_name %></h1>
<body><%= content %></body>

<div class='container'>
<body><%= content %></body>
  <br><br>
  <a href='/'>Back to home</a>
  <br><br>
  <h1><%= $first_name %>'s Stats</h1>
  <table class='table table-striped'>
    <thead>
      <tr>
        <th>Number of Wins</th>
        <th>Number of Losses</th>
        <th>Average Score</th>
        <th>Highest Score</th>
      </tr>
    </thead>
    <tbody>
      <tr>
      % my $query_wins = db->prepare("SELECT * FROM games WHERE (player_one = " . $id . " AND p1_score > p2_score) OR (player_two = " . $id . " AND p2_score > p1_score)");
      % $query_wins->execute;
      % my $wins = $query_wins->rows;

      % my $query_losses = db->prepare("SELECT * FROM games WHERE (player_one = " . $id . " AND p1_score < p2_score) OR (player_two = " . $id . " AND p2_score < p1_score)");
      % $query_losses->execute;
      % my $losses = $query_losses->rows;

      % my $query_scores_1 = db->prepare("SELECT p1_score FROM games WHERE player_one = " . $id);
      % my $query_scores_2 = db->prepare("SELECT p2_score FROM games WHERE player_two = " . $id);
      % $query_scores_1->execute;
      % $query_scores_2->execute;
      % my @scores;
      % while(my @row = $query_scores_1->fetchrow){
      %   push(@scores, $row[0]);
      % }
      % while(my @row = $query_scores_2->fetchrow){
      %   push(@scores, $row[0]);
      % }
      % my $avg = getPlayerAvg(@scores);

      % my $topScore = getPlayerHighest(@scores);

        <td><%= $wins %></td>
        <td><%= $losses %></td>
        <td><%= $avg %></td>
        <td><%= $topScore %></td>


      </tr>
    </tbody>
  </table>

  % my $query_best_score_p1 = db->prepare("SELECT * FROM games WHERE player_one = " . $id . " ORDER BY p1_score DESC LIMIT 1;");
  % my $query_best_score_p2 = db->prepare("SELECT * FROM games WHERE player_two = " . $id . " ORDER BY p2_score DESC LIMIT 1;");
  % $query_best_score_p1->execute;
  % $query_best_score_p2->execute;

  % my @score1 = $query_best_score_p1->fetchrow;
  % my @score2 = $query_best_score_p2->fetchrow;
  % my $bestScore;
  % my $date;
  % my $againstId;

  % if ($score1[3] > $score2[4]){
  %   $bestScore = $score1[3];
  %   $date = $score1[5];
  %   $againstId = $score1[2];
  % } else {
  %   $bestScore = $score2[4];
  %   $date = $score2[5];
  %   $againstId = $score2[1];
  % }

  % my $player_opponent = db->prepare("SELECT * FROM players WHERE id = " . $againstId);
  % $player_opponent->execute;
  % my @opponent = $player_opponent->fetchrow;
  <br><br>
  <p><%= $first_name %>'s best score was <%= $bestScore %> and was acheived on <%= substr $date, 8, 2 %>/<%= substr $date, 5, 2 %>/<%= substr $date, 0, 4 %> against <%= $opponent[1] . ' ' . $opponent[2] %></p>

</div>
